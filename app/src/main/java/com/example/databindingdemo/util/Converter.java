package com.example.databindingdemo.util;

import android.widget.EditText;

import androidx.databinding.InverseMethod;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


public class Converter {

    @InverseMethod("toStringVal")
    public static LiveData<Integer> toInt(EditText editText, String val) {
        MutableLiveData<Integer> data = new MutableLiveData<>();
        data.setValue(Integer.parseInt(val));
        return data;
    }

    public static String toStringVal(EditText editText, LiveData<Integer> val) {
        return String.valueOf(val.getValue());
    }

}
