package com.example.databindingdemo.model;

import android.util.Log;
import android.view.View;

import androidx.databinding.BindingAdapter;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;



public final class DataViewModel extends ViewModel {
    private final MutableLiveData<String> _name = new MutableLiveData<>();
    private final MutableLiveData<Integer> _value = new MutableLiveData<>();
    private final MutableLiveData<Integer> _editValue = new MutableLiveData<>();

    private LiveData<Integer> value;
    private LiveData<String> name;
    private LiveData<Integer> editValue;

    public LiveData<Integer> getEditValue() {
        return editValue;
    }

    public void setEditVal(LiveData<Integer> editValue) {
        this.editValue = editValue;
        try {
            int val = editValue.getValue();
            this._value.setValue(val);
        } catch (Exception e) {
            Log.d("SOME_TAG", "MSG", e);
        }
    }

    public DataViewModel() {
        this._value.setValue(0);
        this._name.setValue("Pankaj");
        this._editValue.setValue(0);
        this.value = this._value;
        this.name = this._name;
        this.editValue = this._editValue;
    }

    public DataViewModel(Integer value, String name, int editVal) {
        this._value.setValue(value);
        this._name.setValue(name);
        this._editValue.setValue(editVal);
        this.value = this._value;
        this.name = this._name;
        this.editValue = this._editValue;
    }

    public LiveData<Integer> getValue() {
        return value;
    }

    public void setValue(LiveData<Integer> value) {
        this.value = value;
    }

    public LiveData<String> getName() {
        return name;
    }

    public void setName(LiveData<String> name) {
        this.name = name;
    }

    public void increase() {
        this._value.setValue((this._value.getValue() != null ? this._value.getValue() : 0) + 1);
    }

    public void decrease() {
        if (this._value.getValue() != null) {
            this._value.setValue((this._value.getValue() > 0  ? this._value.getValue() : 1) - 1);
        }
    }

    @BindingAdapter("android:hideView")
    public static void hideView(View view, int val) {
        view.setVisibility(val > 5 ? View.GONE : View.VISIBLE);
    }

}
